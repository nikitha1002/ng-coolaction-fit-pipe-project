import {
    Pipe,
    PipeTransform
} from '@angular/core';

import { CoolActionFit } from '@actjs.on/cool-action-fit';


@Pipe({
    name: 'filterBy'
})
export class FilterByPipe implements PipeTransform {

    private filter: CoolActionFit.Filter;

    constructor() {
        this.filter = new CoolActionFit.Filter();
    }

    transform<T extends Object>(collection: T[], term: string, ...properties: string[]): T[] {

        const filtered: T[] = [];

        // cause' of asynchronous loading
        if (!collection || !collection.length) {
            return [];
        }

        // do not stop to use the getMaps method if the term is empty, otherwise the cache will never be emptied, which may create unexpected results
        this.filter.getMaps(collection, term, ...properties).forEach(
            (map: CoolActionFit.IFilteredMap) => {
                filtered.push(map.source as T);
            }
        );

        return filtered;
    }

}
