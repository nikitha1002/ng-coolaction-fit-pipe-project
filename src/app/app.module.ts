import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// import { NgCoolActionFitPipeModule } from '@actjs.on/ng-cool-action-fit-pipe';
import { NgCoolActionFitPipeModule } from 'projects/ng-cool-action-fit-pipe/src/public-api';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        NgCoolActionFitPipeModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [

    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
